import puppeteer from "puppeteer";
import {connect} from "./browserstack.conf"
describe("App.js", () => {
  let browser;
  let page;

  beforeAll(async () => {
    // browser = await puppeteer.launch();
    browser = await puppeteer.connect(connect);
    page = await browser.newPage();
  }, 15000);

  it("shows a success message after submitting a form", async () => {
    // await page.goto("http://localhost:5000");
    await page.goto("http://browserstack-poc.s3-website-us-east-1.amazonaws.com/");
    await page.waitForSelector(".form-header");

    await page.click(".form-input__email");
    await page.type(".form-input__email", "username@gmail.com");

    await page.click(".form-input__password");
    await page.type(".form-input__password", "password");

    await page.click(".form-submit-button");

    await page.waitForSelector(".form-success-message");
    const text = await page.$eval(
      ".form-success-message",
      (e) => e.textContent
    );
    
    try{
      expect(text).toContain("You are now signed in.");
      await page.evaluate(_ => {}, `browserstack_executor: ${JSON.stringify({action: 'setSessionStatus',arguments: {status: 'passed',reason: 'shows a success message after submitting a form'}})}`);
    }
    catch {
      await page.evaluate(_ => {}, `browserstack_executor: ${JSON.stringify({action: 'setSessionStatus',arguments: {status: 'failed',reason: 'Test assertion failed'}})}`);
    }

    
  },120000);

  it("shows an error message if authentication fails", async () => {
    // await page.goto("http://localhost:5000");
    await page.goto("http://browserstack-poc.s3-website-us-east-1.amazonaws.com/");
    await page.waitForSelector(".form-header");

    await page.click(".form-input__email");
    await page.type(".form-input__email", "username@gmail.com");

    await page.click(".form-input__password");
    await page.type(".form-input__password", "password123");

    await page.click(".form-submit-button");

    await page.waitForSelector(".form-error-text");
    const text = await page.$eval(".form-error-text", (e) => e.textContent);

    expect(text).toContain("Please enter a correct username/password.");
  },120000);

  afterAll(async() => await browser.close(), 15000);
});

import { Builder, By, Capability, Key, promise, until } from 'selenium-webdriver';
import webdriver from 'selenium-webdriver';

const url = process.env.WEB_URL || "http://browserstack-poc.s3-website-us-east-1.amazonaws.com"

const connect = `http://${process.env.BROWSERSTACK_USERNAME}:${process.env.BROWSERSTACK_ACCESS_KEY}@hub-cloud.browserstack.com/wd/hub`

const capabilities = [
    {
        'device': 'iPhone 11',
        'realMobile': 'true',
        'os_version': '14.0',
        'browserName': 'iPhone',
        'name': 'iphone 11 browser 14.0', // test name
    },
    {
        "os": "Windows",
        "os_version": "10",
        "browserName": "Chrome",
        "browser_version": "latest",
        'name': 'Chrome latest on Windows 10', // test name
    },
    {
        "os": "OS X",
        "os_version": "Big Sur",
        "browserName": "Safari",
        "browser_version": "14.1",
        'name': 'Safari latest on Big Sur', // test name
    },

    {
        "os": "Windows",
        "os_version": "10",
        "browserName": "Chrome",
        "browser_version": "93.0",
        'name': 'Chrome latest on Windows 10', // test name
    },
    {
        "os_version": "14",
        "device": "iPad Pro 12.9 2021",
        "real_mobile": "true",
        "browserName": "iPad",
        'name': 'Ipad browser latest on ipad', // test name
    }
]


const getElementByClassName = async (driver, className, timeout = 5000) => {
    const el = await driver.wait(until.elementLocated(By.className(className)), timeout);
    return await driver.wait(until.elementIsVisible(el), timeout);
};


const runTests = async (capability) => {
    capability["build"] = process.env.BROWSERSTACK_BUILD_NAME || require('../package.json').version
    describe('API.js', function () {
        let driver;

        beforeEach(async function () {
            driver = await new Builder()
                .usingServer(connect)
                .withCapabilities(capability)
                .build();
        }, 300 * 1000);

        afterEach(async function () {
            await driver.quit();
        }, 500 * 1000);

        it('shows a success message after submitting a form', async function () {
            await driver.get(url);

            var emailElement = await getElementByClassName(driver, 'form-input form-input__email');
            emailElement.sendKeys("username@gmail.com");

            var passwordElement = await getElementByClassName(driver, 'form-input__password');
            passwordElement.sendKeys("password");

            var submit = await getElementByClassName(driver, 'form-submit-button');
            submit.click();


            try {
                const successMsg = await driver.wait(until.elementLocated(By.className('form-success-message')));
                const text = await successMsg.getText();
                expect(text).toContain("You are now signed in.");
                await driver.executeScript('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed","reason": "Shows success msg after a login"}}');
            }
            catch (e) {
                console.log(e.stack);
                await driver.executeScript(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed","reason": "unknown error"}}'
                );
            }
        }, 300 * 1000);

        it('shows an error message if authentication fails', async function () {
            await driver.get(url);

            var emailElement = await getElementByClassName(driver, 'form-input form-input__email');
            emailElement.sendKeys("username@gmail.com");

            var passwordElement = await getElementByClassName(driver, 'form-input__password');
            passwordElement.sendKeys("password123");

            var submit = await getElementByClassName(driver, 'form-submit-button');
            submit.click();


            try {
                const errorMsg = await driver.wait(until.elementLocated(By.className('form-error-text')));
                const text = await errorMsg.getText();
                expect(text).toContain("Please enter a correct username/password.");
                await driver.executeScript('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed","reason": "Shows error msg after a wrong login"}}');
            }
            catch (e) {
                console.log(e.stack);
                await driver.executeScript(
                    'browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed","reason": "unknown error"}}'
                );
            }
        }, 300 * 1000);
    });
};

Promise.allSettled([runTests(capabilities[0]), runTests(capabilities[1]), runTests(capabilities[2]), runTests(capabilities[3]), runTests(capabilities[4])]);
// The following caps JSON contains the information based on which your test would run on BrowserStack. You need to specify the browser/OS combination here and pass on the BrowserStack credentials
const caps = {
    'browser': 'chrome',
    'browser_version': 'latest',
    'os': 'osx',
    'os_version': 'big sur',
    'name': 'Puppeteer-jest test on Chrome',
    'build': process.env.BROWSERSTACK_BUILD_NAME || 'puppeteer-jest-build',
    'browserstack.username': process.env.BROWSERSTACK_USERNAME || '',
    'browserstack.accessKey': process.env.BROWSERSTACK_ACCESS_KEY || ''
  };
  
  module.exports = {
    connect: {
      browserWSEndpoint: `wss://cdp.browserstack.com?caps=${encodeURIComponent(JSON.stringify(caps))}`,
    },
    caps
  }
  
const { Builder, until, By } = require('selenium-webdriver');
const percySnapshot = require('@percy/selenium-webdriver');
let chrome = require('chromedriver');

const url = process.env.WEB_URL || "https://ppe-portal.mypearson.com/login"

const connect = `http://${process.env.BROWSERSTACK_USERNAME}:${process.env.BROWSERSTACK_ACCESS_KEY}@hub-cloud.browserstack.com/wd/hub`

const capability = {
    "os": "Windows",
    "os_version": "10",
    "browserName": "Chrome",
    "browser_version": "latest",
    'name': 'Percy MLM with BrowserStack', // test name
}

const getElementById = async (driver, id, timeout = 5000) => {
    const el = await driver.wait(until.elementLocated(By.id(id)), timeout);
    return await driver.wait(until.elementIsVisible(el), timeout);
};

const getElementByClassName = async (driver, className, timeout = 5000) => {
    const el = await driver.wait(until.elementLocated(By.className(className)), timeout);
    return await driver.wait(until.elementIsVisible(el), timeout);
};

(async function runTests() {
    // let driver = await new Builder()
    // .forBrowser('chrome')
    // // .setChromeOptions(new chrome.Options().headless())
    // .build();

    let driver = await new Builder()
        .usingServer(connect)
        .withCapabilities(capability)
        .build();

    try {
        //maximizing the browser to fit the window screen
        driver.manage().window().maximize();
        await driver.get(url);

        //getting the login page
        await percySnapshot(driver, 'Login Page');

        //login to the system as a student
        await driver.executeScript(`document.getElementById("mainButton").style.backgroundColor="#daf0ed"`);
        var emailElement = await driver.findElement(By.id('username'));
        emailElement.sendKeys("ckstu003");

        var passwordElement = await driver.findElement(By.id('password'));
        passwordElement.sendKeys("Password1");

        var submit = await driver.findElement(By.id('mainButton'));
        submit.click();
        //getting the dashboard after student login
        const success = await driver.wait(until.elementLocated(By.className('drag-handler')));
        await percySnapshot(driver, 'Login success Page');

        //using search box 
        var searchElement = await driver.findElement(By.id('keywordTextbox'));
        searchElement.sendKeys("CP05");
        var submitSearch = await driver.findElement(By.className('btn btn-white btn-search-icon'));
        submitSearch.click();
        const searchResult = await driver.wait(until.elementLocated(By.className('drag-handler')));
        await percySnapshot(driver, 'Successful Search Result');

        //main screen toggle view
        var listViewButton = await driver.findElement(By.className('table-col display-toggle-wrapper'));
        listViewButton.click();
        await percySnapshot(driver, 'List view');

        //getting result list from inactive tab
        var inactiveTabElement = await driver.findElement(By.className('tab-text'));
        inactiveTabElement.click();
        const inactiveList = await driver.wait(until.elementLocated(By.className('drag-handler')));
        await percySnapshot(driver, 'Inactive Course List');

        //enroll student in a course
        await driver.navigate().to('https://ppe-portal.mypearson.com/course-home#/tab/active');
        var instructorEnrollButton = await driver.findElement(By.className('tabs-button-wrapper'));
        instructorEnrollButton.click();
        await percySnapshot(driver, 'Enroll In a Course');

        var closeEnrollButton = await driver.findElement(By.className('btn-close dialog-close'));
        closeEnrollButton.click();

        //to add a category
        var addCategoryButton = await driver.findElement(By.id('btnAddGroup'));
        addCategoryButton.click();

        await percySnapshot(driver, 'Add a Category');

        //open user profile
        await driver.navigate().to('https://ppe-portal.mypearson.com/course-home#/tab/active');
        var profileButton = await driver.findElement(By.id('userNameText'));
        profileButton.click();
        await percySnapshot(driver, 'Open Profile DropDown');

        var profile = await driver.findElement(By.className('account-holder'));
        profile.click();
        // var profileElement = await driver.wait(until.elementLocated(By.id('mainButton')));
        await percySnapshot(driver, 'Open Profile');

        var profileClose = await driver.findElement(By.className('fa fa-close'));
        profileClose.click();

        var profileButton = await driver.findElement(By.id('userNameText'));
        profileButton.click();

        var signOut = await driver.findElement(By.className('sign-out-container'));
        await percySnapshot(driver, 'Sign out');
        signOut.click();


        //un authorized login to the system as a student
        // await driver.get(url);

        // await driver.executeScript(`document.getElementById("mainButton").style.backgroundColor="#daf0ed"`);
        // var emailElement = await driver.findElement(By.id('username'));
        // emailElement.sendKeys("abc");

        // var passwordElement = await driver.findElement(By.id('password'));
        // passwordElement.sendKeys("Password");

        // var submit = await driver.findElement(By.id('mainButton'));
        // submit.click();
        // //getting the error
        // await percySnapshot(driver, 'Login Error Page');


    } finally {
        await driver.quit();
    }
})();

/**
 * Dependency Modules
 */
 var assert = require("assert").strict;
 var webdriver = require("selenium-webdriver");
 require("geckodriver");
 
 // Application Server
 const serverUri = "http://localhost:3000/#";
 const appTitle = "React App";
 
 /**
  * Config for Firefox browser (Comment Chrome config when you intent to test in Firefox)
  * @type {webdriver}
  */
 
 var browser = new webdriver.Builder()
  .usingServer()
  .withCapabilities({ browserName: "firefox" })
  .build();
  
  
  const {Builder, By, Key, until} = require('selenium-webdriver');
 
 
  it("shows a success message after submitting a form", async function main() {
 
   await browser.get('http://localhost:3000/#');
   var emailElement = await browser.findElement(By.className('form-input form-input__email'));
   emailElement.sendKeys("username@gmail.com");
   var passwordElement = await browser.findElement(By.className('form-input__password'));
   passwordElement.sendKeys("password");
   await browser.findElement(By.id('submit-button')).click();
   //await browser.wait(until.elementIsVisible(By.className("form-success-message"),200000))
    const sucelement = await browser.findElement(By.className("form-success-message"));
    console.log("HHHHHHHHHHHH"+sucelement.getText());
 
 
 },100000);
 
 
 
 
 
 it("shows an error message if authentication fails", async function main() {
 
 
   await browser.get('http://localhost:3000/');
   var emailElement = await browser.findElement(By.className('form-input form-input__email'));
   emailElement.sendKeys("username@gmail.com");
   var passwordElement = await browser.findElement(By.className('form-input__password'));
   passwordElement.sendKeys("password123");
   await browser.findElement(By.id('submit-button')).click();
    browser.wait(until.elementIsVisible(By.className("form-error-tex"),1000))
 
 
 },10000);
 
  
  /**
   * End of test cases use.
   * Closing the browser and exit.
   */
  after(async function() {
   // End of test use this.
   await browser.quit();
  },10000);
 
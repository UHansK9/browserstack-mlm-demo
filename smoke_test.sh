#!/bin/sh

set +xe
 
# cd "$CI_PROJECT_DIR" || exit 1

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
apt-get update
apt-get install google-chrome-stable -y
npm install
npm install -g @percy/cli
npm install  @percy/selenium-webdriver
npm install selenium-webdriver
npm install chromedriver

percy exec -- node src/snapshots.js

